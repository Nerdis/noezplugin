package de.Nerdis.Noez.Listener;

import de.Nerdis.Noez.Main;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Created by Nerdis on 26.07.2015.
 */

public class PlayerJoinListener implements  Listener{

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e){
        Player p = e.getPlayer();

        p.sendMessage(Main.prefix + ChatColor.AQUA + "Server hosted by " + ChatColor.DARK_RED + "http://noez.de/");
    }

}
