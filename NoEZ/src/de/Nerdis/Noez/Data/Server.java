package de.Nerdis.Noez.Data;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Nerdis on 26.07.2015.
 */

public class Server {
	
	public static Runtime runtime = Runtime.getRuntime();
	
	public static double usedMemory() {
		
		double maxmemory = runtime.maxMemory();
		double usedmemory = runtime.totalMemory();
		double maxm = maxmemory / 1048576.0D;
		
		maxm /= 100.0D;
		
		double usedm = usedmemory / 1048576.0D;
		double auslastung = usedm / maxm;
		
		auslastung *= 100.0D;
		auslastung = Math.round(auslastung);
		auslastung /= 100.0D;
		return auslastung;
		
	}

	public static ChatColor ramCheck() {
		
		double raminpro = usedMemory();
		
		if (raminpro <= 50.0D) {
			return ChatColor.GREEN;
		}
		
		if ((raminpro >= 50.100000000000001D) && (raminpro <= 80.0D)) {
			return ChatColor.YELLOW;
		} 
		
		if ((raminpro >= 80.099999999999994D) && (raminpro <= 100.0D)) {
			return ChatColor.DARK_RED;
		}
		
		return ChatColor.WHITE;
		
	}
	
	public static ChatColor TPSCheck() {
		 
		double tps = Lag.getTPS();
		
		if(tps <= 20.0D){
			return ChatColor.GREEN;
		} else
		
		if((tps >= 15.0D) && (tps <= 20.0D)){
			return ChatColor.YELLOW;
		} else
		
		if((tps >= 10.D) && (tps <= 15.0D)){
			return ChatColor.RED;
		}
		
		return ChatColor.WHITE;
		
	}

	public static void sendRam(Player p) {
		
	      p.sendMessage("�bFreier-RAM: " + ChatColor.GOLD + (runtime.maxMemory() - runtime.totalMemory()) / 1048576L + "MB");
	      p.sendMessage("�bBenuzter-RAM: " + ChatColor.GOLD + runtime.totalMemory() / 1048576L + "MB");
	      p.sendMessage("�bMax-RAM: " + ChatColor.GOLD + runtime.maxMemory() / 1048576L + "MB");
	      p.sendMessage("�bProzent: " + ramCheck() + usedMemory() + "%");
	      
	}
	
	public static void sendTPS(Player p) {
		
		p.sendMessage("�bTPS: " + TPSCheck() + Lag.getTPS());
		
	}

	public static void sendRam(CommandSender sender) {
		
	      sender.sendMessage("�bFreier-RAM: " + ChatColor.GOLD + (runtime.maxMemory() - runtime.totalMemory()) / 1048576L + "MB");
	      sender.sendMessage("�bBenuzter-RAM: " + ChatColor.GOLD + runtime.totalMemory() / 1048576L + "MB");
	      sender.sendMessage("�bMax-RAM: " + ChatColor.GOLD + runtime.maxMemory() / 1048576L + "MB");
	      sender.sendMessage("�bProzent: " + ramCheck() + usedMemory() + "%");
		
	}

	public static void sendTPS(CommandSender sender) {
		
		sender.sendMessage("�bTPS: " + TPSCheck() + Lag.getTPS());
		
	}

}
