package de.Nerdis.Noez;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.Plugin;

import de.Nerdis.Noez.Commands.NoezCommand;
import de.Nerdis.Noez.Data.Lag;
import de.Nerdis.Noez.Listener.PlayerJoinListener;

/**
 * Created by Nerdis on 26.07.2015.
 */

public class Main extends JavaPlugin{

    public static String prefix = ChatColor.GRAY + "[" + ChatColor.GOLD + "Noez" + ChatColor.GRAY + "] ";

    public static Plugin plugin;

    @Override
    public void onEnable() {

        System.out.println("[Noez] started.");

        plugin = this;

        Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(), this);
        
        getCommand("noez").setExecutor(new NoezCommand());

        getServer().getScheduler().scheduleSyncRepeatingTask(this, new Lag(), 100L, 1L);
        
        getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {

                Bukkit.broadcastMessage(prefix +  ChatColor.RED + "Der Server wird bei " + ChatColor.GOLD + "http://noez.de/" + ChatColor.RED + " gehostet!");

            }
        }, 0L, 6000L);

    }

    @Override
    public void onDisable() {

        System.out.println("[Noez] stopped.");

    }

    public static Plugin getPlugin(){
        return plugin;
    }

}
