package de.Nerdis.Noez.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.Nerdis.Noez.Data.Server;

/**
 * Created by Nerdis on 26.07.2015.
 */

public class NoezCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String cmdlabel, String[] args) {

		if(sender instanceof Player) {
			Player p = (Player) sender; 

			if(cmdlabel.equalsIgnoreCase("noez")) {

				if(args.length == 0){

					p.sendMessage(ChatColor.RED + "┌─────────" + ChatColor.GRAY + " [ " + ChatColor.GOLD + "Noez" + ChatColor.GRAY + " ] " + ChatColor.RED + "─────────┐");
					p.sendMessage("§aServerinfos: §c/noez serverinfo");								 
					p.sendMessage(ChatColor.RED + "└─────────" + ChatColor.GRAY + " [ " + ChatColor.GOLD + "Noez" + ChatColor.GRAY + " ] " + ChatColor.RED + "─────────┘");

				}

				if(args.length == 1){

					if(args[0].equalsIgnoreCase("serverinfo")){

						p.sendMessage(ChatColor.RED + "┌─────────" + ChatColor.GRAY + " [ " + ChatColor.GOLD + "Noez" + ChatColor.GRAY + " ] " + ChatColor.RED + "─────────┐");
						Server.sendRam(p);
						Server.sendTPS(p);
						p.sendMessage(ChatColor.RED + "└─────────" + ChatColor.GRAY + " [ " + ChatColor.GOLD + "Noez" + ChatColor.GRAY + " ] " + ChatColor.RED + "─────────┘");

					}
				}
				
				if(args.length > 1){
					
					p.sendMessage(ChatColor.RED + "┌─────────" + ChatColor.GRAY + " [ " + ChatColor.GOLD + "Noez" + ChatColor.GRAY + " ] " + ChatColor.RED + "─────────┐");
					p.sendMessage("§aServerinfos: §c/noez serverinfo");								 
					p.sendMessage(ChatColor.RED + "└─────────" + ChatColor.GRAY + " [ " + ChatColor.GOLD + "Noez" + ChatColor.GRAY + " ] " + ChatColor.RED + "─────────┘");
					
					
				}
			}


		} else {

			if(cmdlabel.equalsIgnoreCase("noez")) {

				if(args.length == 0){

					sender.sendMessage(ChatColor.RED + "┌─────────" + ChatColor.GRAY + " [ " + ChatColor.GOLD + "Noez" + ChatColor.GRAY + " ] " + ChatColor.RED + "─────────┐");
					sender.sendMessage("§aServerinfos: §c/noez serverinfo");								 
					sender.sendMessage(ChatColor.RED + "└─────────" + ChatColor.GRAY + " [ " + ChatColor.GOLD + "Noez" + ChatColor.GRAY + " ] " + ChatColor.RED + "─────────┘");

				}

				if(args.length == 1){

					if(args[0].equalsIgnoreCase("serverinfo")){

						sender.sendMessage(ChatColor.RED + "┌─────────" + ChatColor.GRAY + " [ " + ChatColor.GOLD + "Noez" + ChatColor.GRAY + " ] " + ChatColor.RED + "─────────┐");
						Server.sendRam(sender);
						Server.sendTPS(sender);
						sender.sendMessage(ChatColor.RED + "└─────────" + ChatColor.GRAY + " [ " + ChatColor.GOLD + "Noez" + ChatColor.GRAY + " ] " + ChatColor.RED + "─────────┘");

					}
				}
			}
		}

		return true;
	}

}
